
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [stage].[ResourceAllocation](
	[Name] [nvarchar](255) NULL,
	[Project] [nvarchar](255) NULL,
	[Initiative] [nvarchar](255) NULL,
	[Month] [nvarchar](255) NULL,
	[Year] [nvarchar](255) NULL,
	[Percentage] [numeric](32, 16) NULL
) ON [PRIMARY]

GO


