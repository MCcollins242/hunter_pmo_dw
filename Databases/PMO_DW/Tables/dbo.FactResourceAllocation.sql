
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FactResourceAllocation](
	[ProjectKey] [int] NULL,
	[EmployeeKey] [int] NULL,
	[MonthKey] [nvarchar](6) NULL,
	[Percentage] [numeric](32, 16) NULL
) ON [PRIMARY]

GO


