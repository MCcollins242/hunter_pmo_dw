
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DimPeakAllocation](
	[EmployeeKey] [int] NOT NULL,
	[EmployeeName] [nvarchar](255) NULL,
	[MonthKey] [nvarchar](6) NOT NULL,
	[MonthName] [nvarchar](9) NOT NULL,
	[PeakAllocation] [numeric](32, 18) NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
	[IsCurrent] [bit] NULL
) ON [PRIMARY]

GO


