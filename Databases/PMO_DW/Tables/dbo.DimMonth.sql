
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DimMonth](
	[MonthKey] [nvarchar](6) NOT NULL,
	[Year] [smallint] NOT NULL,
	[HalfYear] [tinyint] NOT NULL,
	[HalfYearName] [nchar](2) NOT NULL,
	[Quarter] [tinyint] NOT NULL,
	[QuarterName] [nchar](2) NOT NULL,
	[Month] [tinyint] NOT NULL,
	[MonthName] [nvarchar](9) NOT NULL,
	[YearMonth] [int] NOT NULL,
	[YearandMonthName] [nvarchar](14) NOT NULL
) ON [PRIMARY]

GO


