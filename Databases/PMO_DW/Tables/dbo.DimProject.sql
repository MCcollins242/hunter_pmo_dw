SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DimProject](
	[ProjectKey] [int] IDENTITY(1,1) NOT NULL,
	[Project] [nvarchar](255) NULL,
	[Initiative] [nvarchar](255) NULL,
	[CostBaseline] [int] NULL,
	[FTEBaseline] [numeric](32, 18) NULL,
	[BaselineDate] [date] NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
	[IsCurrent] [bit] NULL,
	[Status] [nvarchar](16) NULL,
	[Duration] [nvarchar](6) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProjectKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


