
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vDimPeakAllocation]
AS

SELECT [EmployeeKey]
      ,[EmployeeName]
      ,[MonthKey]
      ,[MonthName]
      ,[PeakAllocation] AS [Peak Allocation]
  FROM [ResourceAllocation].[dbo].[DimPeakAllocation]
GO


