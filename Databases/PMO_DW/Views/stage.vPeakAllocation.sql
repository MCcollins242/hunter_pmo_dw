
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [stage].[vPeakAllocation]
AS
SELECT de.EmployeeKey,
	   de.EmployeeName,
	   dm.MonthKey,
	   dm.[MonthName]
		
FROM [dbo].[DimEmployee] AS de, [dbo].[DimMonth] AS dm








GO


