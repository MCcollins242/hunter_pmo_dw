
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vDimMonth]
AS
SELECT [MonthKey]
      ,[Year]
      ,[HalfYear]
      ,[HalfYearName] AS [Half Year Name]
      ,[Quarter]
      ,[QuarterName] AS [Quarter Name]
      ,[Month]
      ,[MonthName] AS [Month Name]
      ,[YearMonth] 
      ,[YearandMonthName] AS [Year and Month Name]
  FROM [ResourceAllocation].[dbo].[DimMonth]
GO


