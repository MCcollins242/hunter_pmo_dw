SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vDimProject]
AS

SELECT [ProjectKey]
      ,[Project]
      ,[Initiative]
      ,[CostBaseline] AS [Cost Baseline]
      ,[FTEBaseline] AS [FTE Baseline]
	  ,[BaselineDate] AS [Baseline Date]
	  ,[Status]
	  ,[Duration]
  FROM [dbo].[DimProject]


GO


