
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vDimEmployee]
AS
SELECT [EmployeeKey]
      ,[EmployeeName] AS [Employee Name]
	  ,[Function]
      ,[FTERate]
      ,[Status]
  FROM [ResourceAllocation].[dbo].[DimEmployee]


GO


