
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [stage].[vFactResourceAllocation]
AS
SELECT dp.ProjectKey,
	   de.EmployeeKey,
	   dm.MonthKey,
	   Percentage
		
FROM [stage].[ResourceAllocation] AS ra

LEFT OUTER JOIN [dbo].[DimProject] AS dp
	ON dp.Project = ra.Project
		AND IIF(ra.Initiative = '','N/A', ra.Initiative) = dp.Initiative

LEFT OUTER JOIN [dbo].[DimEmployee] AS de
	ON ra.Name = de.EmployeeName

LEFT OUTER JOIN [dbo].[DimMonth] AS dm
	ON dm.MonthName = ra.Month
		AND dm.Year = ra.Year





GO


