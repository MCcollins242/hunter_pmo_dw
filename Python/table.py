from flask_table import Table, Col, LinkCol, DatetimeCol

class ProjectTable(Table):
    classes = ['table table-hover']
    thead_classes = ['thead-dark']
    ID = Col('ID', show=False)
    projectname = Col('Project Name')
    projectmanager = Col('Project Manager')
    projectstatus = Col('Project Status')
    modifieddate = DatetimeCol('Last Updated', datetime_format="dd/MM/yyyy HH:mm")
    edit = LinkCol('Edit', 'edit', url_kwargs=dict(ID='ID'), anchor_attrs={'type': 'button', 'class': 'btn btn-dark'})


