from flask import Flask, render_template, url_for, request, redirect, flash
from flask_table import Table, Col
import pyodbc
from forms import ProjectForm, SearchForm
from table import ProjectTable

connection = pyodbc.connect('Driver={SQL Server};'
                            r'Server=HBI01SQL02\BI;'
                            'Database=PMO_DW;'
                            'Trusted_Connection=yes;')
cursor = connection.cursor()

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'


@app.route('/projectlog', methods=['GET','POST'])
@app.route('/projectlog/view', methods=['GET','POST'])
def view():
    cursor.execute("SELECT ID, [projectname], [projectmanager], [projectstatus], [modifieddate] FROM [PMO_DW].[stage].[ProjectLog] ORDER BY ID")
    rows = cursor.fetchall()
    table = ProjectTable(rows)
    return render_template('view.html', title='View', table=table)


@app.route('/projectlog/search', methods=['GET','POST'])
def search():
    form = SearchForm()
    if form.validate_on_submit():
        flash(f'Showing search results for: ' + form.searchtext.data, 'info')
        search = form.searchtext.data
        return redirect( url_for('results', search=search))
    return render_template('search.html', title='Search', form=form)

@app.route('/projectlog/results', methods=['GET','POST'])
def results():
    x = request.args.get('search', None)
    query = '''
            DECLARE @SearchWord nvarchar(100)
            DECLARE @SearchQuery nvarchar(100)
            SET @SearchWord = ?
            SET @SearchQuery = '%' + @SearchWord + '%'

            SELECT *
            FROM [PMO_DW].[stage].[ProjectLog]
            WHERE [projectname] LIKE @SearchQuery
                OR [projectmanager] LIKE @SearchQuery
                OR [projectstatus] LIKE @SearchQuery
            '''
    cursor.execute(query,[x])
    rows = cursor.fetchall()
    table = ProjectTable(rows)
    return render_template('results.html', title='View', table=table)


@app.route('/projectlog/new', methods=['GET','POST'])
def new():
    form = ProjectForm(projectscore=0, approvedbudget=0, projectedspend=0, actualspend=0, projectedsaving=0, projectedrevenue=0)
    if form.validate_on_submit():
        flash(f'Entry added Successfully', 'success')
        query = "INSERT INTO [PMO_DW].[stage].[ProjectLog] VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,GETDATE())"
        param = form.projectname.data, form.projectmanager.data, form.sponsor.data, form.ragstatus.data, form.start.data, form.end.data, form.projectstatus.data, form.approvaldate.data, form.projectphase.data, form.projectcode.data, form.keydriver.data, form.alignmentstrategy.data, form.projectscore.data, form.approvedbudget.data, form.projectedspend.data, form.actualspend.data, form.projectedsaving.data, form.projectedrevenue.data, form.ragcomments.data
        cursor.execute(query,param)
        connection.commit()
        return redirect( url_for('view'))
    return render_template('new.html', title='New', form=form)


@app.route('/projectlog/edit/<int:ID>', methods=['GET','POST'])
def edit(ID):
    x = ID
    cursor.execute("SELECT * FROM [PMO_DW].[stage].[ProjectLog] WHERE ID = ?",x)
    row = cursor.fetchall()
    form = ProjectForm(projectname=row[0].projectname, projectmanager=row[0].projectmanager, sponsor=row[0].sponsor, projectstatus=row[0].projectstatus, start=row[0].start, end=row[0].end, approvaldate=row[0].approvaldate, ragstatus=row[0].ragstatus, projectphase=row[0].projectphase, projectcode=row[0].projectcode, keydriver=row[0].keydriver, alignmentstrategy=row[0].alignmentstrategy, projectscore=row[0].projectscore, approvedbudget=row[0].approvedbudget, projectedspend=row[0].projectedspend, actualspend=row[0].actualspend, projectedsaving=row[0].projectedsaving, projectedrevenue=row[0].projectedrevenue, ragcomments=row[0].ragcomments)
    if form.validate_on_submit():
        flash(f'Entry updated Successfully', 'success')
        query = "UPDATE [PMO_DW].[stage].[ProjectLog] SET [projectname] = ?, [projectmanager] = ?, [sponsor] = ?, [ragstatus] = ?, [start] = ?, [end] = ?, [projectstatus] = ?, [approvaldate] = ?,  [projectphase] = ?, [projectcode] = ?, [keydriver] = ?, [alignmentstrategy] = ?, [projectscore] = ?, [approvedbudget] = ?, [projectedspend] = ?, [actualspend] = ?, [projectedsaving] = ?, [projectedrevenue] = ?, [ragcomments] = ?, [modifieddate] = GETDATE() WHERE [ID] = ?"    
        param = form.projectname.data, form.projectmanager.data, form.sponsor.data, form.ragstatus.data, form.start.data, form.end.data, form.projectstatus.data, form.approvaldate.data, form.projectphase.data, form.projectcode.data, form.keydriver.data, form.alignmentstrategy.data, form.projectscore.data, form.approvedbudget.data, form.projectedspend.data, form.actualspend.data, form.projectedsaving.data, form.projectedrevenue.data, form.ragcomments.data, x
        cursor.execute(query,param)
        connection.commit()
        return redirect( url_for('view'))
    return render_template('edit.html', title='Edit', form=form, row=row)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)


#python route.py