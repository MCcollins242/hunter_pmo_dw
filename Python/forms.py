from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField, TextAreaField, DecimalField, StringField, DateField
from wtforms.fields.html5 import DateField
from wtforms.validators import InputRequired, Length, EqualTo, Regexp, NoneOf, NumberRange


class ProjectForm(FlaskForm):
    projectname = StringField('Project Name*',
                           validators=[InputRequired(), Length(min=1, max=100)])
    ragstatus = SelectField('RAG Status',
                           choices=[('',''),('Red','Red'),('Amber','Amber'),('Green','Green')])
    projectmanager = StringField('Project Manager*',
                           validators=[InputRequired(), Length(min=1, max=50)])
    sponsor = StringField('Sponsor*',
                           validators=[InputRequired(), Length(min=1, max=50)])
    start = StringField('Start*',
                           validators=[InputRequired(), Regexp('Q[1-4] FY[1-2][0-9]', message="Must be in format Q* FY**")])
    end = StringField('End*',
                           validators=[InputRequired(), Regexp('Q[1-4] FY[1-2][0-9]', message="Must be in format Q* FY**")])
    projectstatus = SelectField('Project Status*',
                           choices=[('---Please Select---','---Please Select---'),('Approved','Approved'),('Pending Approval','Pending Approval'),('Closed','Closed'),('Complete','Complete')], validators=[NoneOf('---Please Select---', message="Please select a valid value")])
    approvaldate = StringField('Approval Date*', validators=[InputRequired(), Regexp('[0-3][0-9]/[0-1][0-9]/20[1-2][0-9]', message="Must be in format dd/MM/yyyy")])
    projectphase = StringField('Project Phase*',
                           validators=[InputRequired(), Length(min=1, max=50)])
    projectcode = StringField('Project Code',
                           validators=[Length(min=0, max=10)])   
    keydriver = StringField('Key Driver*',
                           validators=[InputRequired(), Length(min=1, max=250)])
    alignmentstrategy = StringField('Alignment to Strategy*',
                           validators=[InputRequired(), Length(min=1, max=250)])
    ragcomments = TextAreaField('RAG Status Comments',
                        validators=[Length(min=0, max=2000)], render_kw={"rows":10})
    projectscore = StringField('Project Score')
    approvedbudget = StringField('Approved Budget (GBP)')
    projectedspend = StringField('Projected Project Spend (GBP)')
    actualspend = StringField('Actual Project Spend to Date (GBP)')
    projectedsaving = StringField('Projected Cost Saving (GBP)')
    projectedrevenue = StringField('Projected Incremental Value (GBP)')
    submit = SubmitField('Submit')

class SearchForm(FlaskForm):
    searchtext = StringField('Search')
    submit = SubmitField('Submit')
